# == Schema Information
#
# Table name: articles
#
#  id         :integer          not null, primary key
#  status     :string(255)
#  title      :string(255)
#  body       :text
#  created_at :datetime
#  updated_at :datetime
#

class Article < ActiveRecord::Base
end
