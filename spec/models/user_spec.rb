require "spec_helper"

describe User do
  let(:user) { User.new(username: "joeblow", email: "joe@blow.com", password: "test1234") }

  describe "email validations" do
    it "requires an email" do
      expect(user).to be_valid
      user.email = nil
      expect(user).to_not be_valid
    end

    it "requires email to be unique" do
      User.create username: "joeblow2", email: "joe@blow.com", password: "test1234"
      expect(user).to_not be_valid
    end
  end

  describe "username validations" do
    it "requires a username" do
      expect(user).to be_valid
      user.username = nil
      expect(user).to_not be_valid
    end

    it "requires username to be unique" do
      User.create username: "joeblow", email: "joe2@blow.com", password: "test1234"
      expect(user).to_not be_valid
    end
  end

  describe "password validations" do
    it "requires password to be at least 8 characters" do
      user.password = "test123"
      expect(user).to_not be_valid
    end

    it "requires password to be less than 12 characters" do
      user.password = "test123412345"
      expect(user).to_not be_valid
    end
  end
end
