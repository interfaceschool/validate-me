require "spec_helper"

describe Article do
  let(:article) { Article.new(title: "Test Article", status: "draft") }

  describe "title validations" do
    it "requires a title" do
      article.title = nil
      expect(article).to_not be_valid
    end

    it "requires title to be unique" do
      Article.create title: "Test Article", status: "draft"
      expect(article).to_not be_valid
    end
  end

  describe "status validations" do
    it "allows 'draft', 'pending', or 'published'" do
      %w(draft pending published).each do |status|
        article.status = status
        expect(article).to be_valid
      end
    end

    it "does not allow anything else" do
      %w(review dumb 3).each do |something_else|
        article.status = something_else
        expect(article).to_not be_valid
      end
    end
  end
end
